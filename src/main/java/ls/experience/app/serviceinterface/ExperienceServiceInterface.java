package ls.experience.app.serviceinterface;

import java.util.Collection;

import ls.experience.app.entity.Experience;

public interface ExperienceServiceInterface {
	   public abstract void createExperience(Experience product);
	   public abstract void updateExperience(Long id, Experience profile);
	   public abstract void deleteExperience(Long id);
	   public abstract Collection<Experience> getExperience();
}
