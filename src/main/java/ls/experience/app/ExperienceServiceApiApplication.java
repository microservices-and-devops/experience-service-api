package ls.experience.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExperienceServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExperienceServiceApiApplication.class, args);
	}

}
