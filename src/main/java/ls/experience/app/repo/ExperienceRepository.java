package ls.experience.app.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ls.experience.app.entity.Experience;
@Repository
public interface ExperienceRepository extends PagingAndSortingRepository<Experience, Long>{
	List<Experience> findAll();
	List<Experience> findByIdIn(List<Long> id);
}
