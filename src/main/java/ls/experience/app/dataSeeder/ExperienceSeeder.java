package ls.experience.app.dataSeeder;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import ls.experience.app.entity.Experience;
import ls.experience.app.repo.*;

@Component
public class ExperienceSeeder implements CommandLineRunner{
	@Autowired
	ExperienceRepository experienceRepo;
	int MAX_ITEM = 200;
	@Override
	public void run(String... args) throws Exception {
		Faker faker = new Faker(new Random(50));
		
		
		for (int i=0; i < MAX_ITEM; i++) {
			Experience experience = new Experience();
			
			experience.setTitle(faker.job().title());
			experience.setDescription(faker.job().title());
			experience.setDesignation(faker.job().position());
			experience.setResponsibilities(faker.job().position());
			experience.setStartDate(faker.date().birthday());
			experience.setEndDate(faker.date().birthday());
			experience.setStatus(true);
			experience.setDeleteFlag(false);
			experience.setCreatedAt(faker.date().birthday());
			experience.setUpdatedAt(faker.date().birthday());
			
			experienceRepo.save(experience);
		}
	}
	
}
